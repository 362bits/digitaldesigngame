#include <iostream>
#include "types.hpp"

int main() {
    using std::cout; using std::endl;
    
    cout << "Size of byte = " << sizeof(byte) << "\n";
    cout << "Size of ushort = " << sizeof(uint16) << "\n";
    cout << "Size of uint = " << sizeof(uint32) << "\n";
    cout << "Size of ulong = " << sizeof(uint64) << "\n";

    return 0;
}