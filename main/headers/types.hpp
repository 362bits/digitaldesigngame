#pragma once

typedef unsigned char byte;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long uint64;

typedef signed char uint8;
typedef signed short uint16;
typedef signed int uint32;
typedef signed long uint64;