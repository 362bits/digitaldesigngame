#pragma once
#include "types.hpp"
#include <string>


class Memory {
public:
    Memory(uint16 SIZE = 256);
    ~Memory();

    unsigned short Allocate(uint16 length);
    bool Free(uint16 addr);

private:
    uint16 CAPACITY;
    byte* blocks;
};